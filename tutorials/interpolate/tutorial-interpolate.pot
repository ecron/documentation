msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-04-28 20:51+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: interpolate-f20.svg:52(format) interpolate-f19.svg:49(format)
#: interpolate-f18.svg:49(format) interpolate-f17.svg:49(format)
#: interpolate-f16.svg:49(format) interpolate-f15.svg:49(format)
#: interpolate-f14.svg:49(format) interpolate-f13.svg:49(format)
#: interpolate-f12.svg:49(format) interpolate-f11.svg:49(format)
#: interpolate-f10.svg:49(format) interpolate-f09.svg:49(format)
#: interpolate-f08.svg:49(format) interpolate-f07.svg:49(format)
#: interpolate-f05.svg:49(format) interpolate-f04.svg:49(format)
#: interpolate-f03.svg:49(format) interpolate-f02.svg:49(format)
#: interpolate-f01.svg:49(format)
msgid "image/svg+xml"
msgstr ""

#: interpolate-f11.svg:117(tspan) interpolate-f04.svg:117(tspan)
#: interpolate-f02.svg:126(tspan)
#, no-wrap
msgid "Exponent: 0.0"
msgstr ""

#: interpolate-f11.svg:121(tspan) interpolate-f04.svg:121(tspan)
#: interpolate-f02.svg:130(tspan)
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr ""

#: interpolate-f11.svg:125(tspan) interpolate-f04.svg:125(tspan)
#: interpolate-f02.svg:134(tspan)
#, no-wrap
msgid "Interpolation Method: 2"
msgstr ""

#: interpolate-f11.svg:129(tspan) interpolate-f04.svg:129(tspan)
#: interpolate-f02.svg:138(tspan)
#, no-wrap
msgid "Duplicate Endpaths: unchecked"
msgstr ""

#: interpolate-f11.svg:133(tspan) interpolate-f04.svg:133(tspan)
#: interpolate-f02.svg:142(tspan)
#, no-wrap
msgid "Interpolate Style: unchecked"
msgstr ""

#: tutorial-interpolate.xml:7(title)
msgid "Interpolate"
msgstr ""

#: tutorial-interpolate.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-interpolate.xml:13(para)
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr ""

#: tutorial-interpolate.xml:18(title)
msgid "Introduction"
msgstr ""

#: tutorial-interpolate.xml:19(para)
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two "
"or more selected paths. It basically means that it “fills in the gaps” "
"between the paths and transforms them according to the number of steps given."
msgstr ""

#: tutorial-interpolate.xml:20(para)
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <menuchoice><guimenu>Extensions</"
"guimenu><guisubmenu>Generate From Path</guisubmenu><guimenuitem>Interpolate</"
"guimenuitem></menuchoice> from the menu."
msgstr ""

#: tutorial-interpolate.xml:21(para)
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <menuchoice><guimenu>Path</guimenu><guimenuitem>Object to Path</"
"guimenuitem></menuchoice> or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>C</keycap></"
"keycombo>. If your objects are not paths, the extension will do nothing."
msgstr ""

#: tutorial-interpolate.xml:25(title)
msgid "Interpolation between two identical paths"
msgstr ""

#: tutorial-interpolate.xml:26(para)
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""

#: tutorial-interpolate.xml:27(para) tutorial-interpolate.xml:51(para)
msgid "For example, take the following two paths:"
msgstr ""

#: tutorial-interpolate.xml:36(para)
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr ""

#: tutorial-interpolate.xml:45(para)
msgid ""
"As can be seen from the above result, the space between the two circle-"
"shaped paths has been filled with 6 (the number of interpolation steps) "
"other circle-shaped paths. Also note that the extension groups these shapes "
"together."
msgstr ""

#: tutorial-interpolate.xml:49(title)
msgid "Interpolation between two different paths"
msgstr ""

#: tutorial-interpolate.xml:50(para)
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by "
"the Interpolation Steps value."
msgstr ""

#: tutorial-interpolate.xml:60(para)
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr ""

#: tutorial-interpolate.xml:69(para)
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""

#: tutorial-interpolate.xml:71(para)
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is "
"important. To find the starting node of a path, select the path, then choose "
"the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The "
"first node that is selected is the starting node of that path."
msgstr ""

#: tutorial-interpolate.xml:73(para)
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""

#: tutorial-interpolate.xml:82(para)
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr ""

#: tutorial-interpolate.xml:91(para)
msgid ""
"Now, notice the changes in the interpolation result when the triangle path "
"is mirrored so the starting node is in a different position:"
msgstr ""

#: tutorial-interpolate.xml:110(title)
msgid "Interpolation Method"
msgstr ""

#: tutorial-interpolate.xml:111(para)
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in "
"the way that they calculate the curves of the new shapes. The choices are "
"either Interpolation Method 1 or 2."
msgstr ""

#: tutorial-interpolate.xml:113(para)
msgid ""
"In the examples above, we used Interpolation Method 2, and the result was:"
msgstr ""

#: tutorial-interpolate.xml:122(para)
msgid "Now compare this to Interpolation Method 1:"
msgstr ""

#: tutorial-interpolate.xml:131(para)
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""

#: tutorial-interpolate.xml:136(title)
msgid "Exponent"
msgstr ""

#: tutorial-interpolate.xml:138(para)
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 0 makes the spacing between the "
"copies all even."
msgstr ""

#: tutorial-interpolate.xml:140(para)
msgid "Here is the result of another basic example with an exponent of 0."
msgstr ""

#: tutorial-interpolate.xml:149(para)
msgid "The same example with an exponent of 1:"
msgstr ""

#: tutorial-interpolate.xml:158(para)
msgid "with an exponent of 2:"
msgstr ""

#: tutorial-interpolate.xml:167(para)
msgid "and with an exponent of -1:"
msgstr ""

#: tutorial-interpolate.xml:176(para)
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""

#: tutorial-interpolate.xml:178(para)
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 1:"
msgstr ""

#: tutorial-interpolate.xml:189(title)
msgid "Duplicate Endpaths"
msgstr ""

#: tutorial-interpolate.xml:190(para)
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""

#: tutorial-interpolate.xml:194(title)
msgid "Interpolate Style"
msgstr ""

#: tutorial-interpolate.xml:195(para)
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each "
"step. So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""

#: tutorial-interpolate.xml:197(para)
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr ""

#: tutorial-interpolate.xml:207(para)
msgid "Interpolate Style also affects the stroke of a path:"
msgstr ""

#: tutorial-interpolate.xml:216(para)
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr ""

#: tutorial-interpolate.xml:227(title)
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr ""

#: tutorial-interpolate.xml:229(para)
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was "
"not possible to create a gradient other than linear (straight line) or "
"radial (round). However, it could be faked using the Interpolate extension "
"and Interpolate Style. A simple example follows — draw two lines of "
"different strokes:"
msgstr ""

#: tutorial-interpolate.xml:237(para)
msgid "And interpolate between the two lines to create your gradient:"
msgstr ""

#: tutorial-interpolate.xml:249(title)
msgid "Conclusion"
msgstr ""

#: tutorial-interpolate.xml:250(para)
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful "
"tool. This tutorial covers the basics of this extension, but experimentation "
"is the key to exploring interpolation further."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-interpolate.xml:0(None)
msgid "translator-credits"
msgstr ""
