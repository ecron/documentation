msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2020-04-28 20:51+0200\n"
"PO-Revision-Date: 2010-10-11 10:15+0700\n"
"Last-Translator: @CameliaGirls Translation Team <cameliagirls@googlegroups."
"com>\n"
"Language-Team: Indonesian <id@li.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: elements-f15.svg:452(format) elements-f14.svg:173(format)
#: elements-f13.svg:102(format) elements-f12.svg:445(format)
#: elements-f11.svg:48(format) elements-f10.svg:48(format)
#: elements-f09.svg:48(format) elements-f08.svg:206(format)
#: elements-f07.svg:91(format) elements-f06.svg:91(format)
#: elements-f05.svg:48(format) elements-f04.svg:48(format)
#: elements-f03.svg:48(format) elements-f02.svg:48(format)
#: elements-f01.svg:48(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: elements-f12.svg:710(tspan)
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Random Ant &amp; 4WD"

#: elements-f12.svg:721(tspan)
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "Gambar SVG dibuat oleh Andrew Fitzsimon"

#: elements-f12.svg:726(tspan)
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Bagian dari Open Clip Art Library"

#: elements-f12.svg:731(tspan)
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr "http://www.openclipart.org/"

#: elements-f04.svg:79(tspan)
#, no-wrap
msgid "BIG"
msgstr "BESAR"

#: elements-f04.svg:93(tspan)
#, no-wrap
msgid "small"
msgstr "kecil"

#: elements-f01.svg:88(tspan)
#, no-wrap
msgid "Elements"
msgstr "Elemen"

#: elements-f01.svg:101(tspan)
#, no-wrap
msgid "Principles"
msgstr "Prinsip"

#: elements-f01.svg:114(tspan) tutorial-elements.xml:96(title)
#, no-wrap
msgid "Color"
msgstr "Warna"

#: elements-f01.svg:127(tspan) tutorial-elements.xml:33(title)
#, no-wrap
msgid "Line"
msgstr "Garis"

#: elements-f01.svg:140(tspan) tutorial-elements.xml:49(title)
#, no-wrap
msgid "Shape"
msgstr "Bentuk"

#: elements-f01.svg:153(tspan) tutorial-elements.xml:79(title)
#, no-wrap
msgid "Space"
msgstr "Ruang"

#: elements-f01.svg:166(tspan) tutorial-elements.xml:113(title)
#, no-wrap
msgid "Texture"
msgstr "Tekstur"

#: elements-f01.svg:179(tspan) tutorial-elements.xml:128(title)
#, no-wrap
msgid "Value"
msgstr "Value"

#: elements-f01.svg:192(tspan) tutorial-elements.xml:65(title)
#, no-wrap
msgid "Size"
msgstr "Ukuran"

#: elements-f01.svg:205(tspan) tutorial-elements.xml:150(title)
#, no-wrap
msgid "Balance"
msgstr "Keseimbangan"

#: elements-f01.svg:218(tspan) tutorial-elements.xml:166(title)
#, no-wrap
msgid "Contrast"
msgstr "Kontras"

#: elements-f01.svg:231(tspan) tutorial-elements.xml:179(title)
#, no-wrap
msgid "Emphasis"
msgstr "Empasis"

#: elements-f01.svg:244(tspan) tutorial-elements.xml:194(title)
#, no-wrap
msgid "Proportion"
msgstr "Proporsi"

#: elements-f01.svg:257(tspan) tutorial-elements.xml:208(title)
#, no-wrap
msgid "Pattern"
msgstr "Pola"

#: elements-f01.svg:271(tspan) tutorial-elements.xml:222(title)
#, no-wrap
msgid "Gradation"
msgstr "Gradiasi"

#: elements-f01.svg:284(tspan) tutorial-elements.xml:240(title)
#, no-wrap
msgid "Composition"
msgstr "Komposisi"

#: elements-f01.svg:297(tspan)
#, no-wrap
msgid "Overview"
msgstr "Tampak kilas"

#: tutorial-elements.xml:7(title)
#, fuzzy
msgid "Elements of design"
msgstr "Elemen dari Desain"

#: tutorial-elements.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-elements.xml:13(para)
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"Tutorial ini akan mendemonstrasikan elemen dan prinsip desain yang biasanya "
"diajarkan pada murid seni untuk memahami bermacam properti yang digunakan "
"dalam membuat karya seni. Ini bukanlah daftar yang baku, jadi silahkan "
"tambah, keluarkan, atau mengkombinasikannya untuk membuat tutorial ini lebih "
"komperhensif."

#: tutorial-elements.xml:29(title)
msgid "Elements of Design"
msgstr "Elemen dari Desain"

#: tutorial-elements.xml:30(para)
msgid "The following elements are the building blocks of design."
msgstr "Elemen-elemen berikut adalah yang membangun sebuah desain."

#: tutorial-elements.xml:34(para)
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"Sebuah garis didefinisikan sebagai tanda dengan panjang dan arah, dihasilkan "
"dengan poin yang bergerak pada sebuah bidang. Sebuah garis bisa bervariasi "
"panjangnya, lebarnya, arahnya, kurvaturnya, dan warnanya. Garis bisa dalam "
"dua dimensi (misalnya garis pensil pada kertas), atau tiga dimensi."

#: tutorial-elements.xml:50(para)
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"Sebuah figur datar, sebuah bentuk, dihasilkan saat sebuah garis mengelilingi "
"sebuah ruang. Perubahan warna atau bayangan bisa mendefinisikan sebuah "
"bentuk. Bentuk bisa dibagi dalam beberapa tipe: geometrik (persegi, segi "
"tiga, lingkaran) dan organik (iregular pada garis luarnya)."

#: tutorial-elements.xml:66(para)
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"Ini menunjuk pada variasi dalam proporsi obyek, garis, atau bentuk. Terdapat "
"ragam ukuran dalam obyek baik yang nyata maupun khayalan."

#: tutorial-elements.xml:80(para)
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"Ruang adalah daerah kosong atau terbuka diantara, disekitar, diatas, "
"dibawah, atau didalam obyek. Sebuah bentuk dibentuk oleh ruang disekitar dan "
"didalamnya. Ruang sering disebut tiga-dimensi atau dua-dimensi. Ruang "
"positif diisi oleh sebuah bentuk, sedangkan ruang negatif dikelilingi oleh "
"bentuk."

#: tutorial-elements.xml:97(para)
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"Warna adalah karakter yang tampak dari sebuah permukaan tergantung dari "
"panjang gelombang cahaya yang dipantulkan darinya. Warna memiliki tiga "
"dimensi: HUE (nama lain dari warna, diindikasi dari namanya misalnya merah "
"atau kuning), VALUE (terang atau gelap), INTENSITY (pendar atau kelam)."

#: tutorial-elements.xml:114(para)
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"Tekstur adalah bagaimana rasa permukaannya (tekstur sebenarnya) atau "
"bagaimana rasa itu terlihat (tekstur implikasi). Tekstur dijelaskan dengan "
"kata seperti kasar, lembut, atau gelombang."

#: tutorial-elements.xml:129(para)
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"Value adalah seberapa gelap atau seberapa terang sesuatu terlihat. Kita bisa "
"mendapatkan value dari sebuah warna dengan menambahkan hitam atau putih pada "
"warna. Chiaroscuro menggunakan value dalam sebuah gambar dengan memberikan "
"kontras gelap atau terang yang dramatis dalam komposisi."

#: tutorial-elements.xml:146(title)
msgid "Principles of Design"
msgstr "Prinsip dari Desain"

#: tutorial-elements.xml:147(para)
msgid "The principles use the elements of design to create a composition."
msgstr "Prinsip penggunaan elemen desain untuk menghasilkan komposisi."

#: tutorial-elements.xml:151(para)
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"Keseimbangan adalah rasa dari melihat sesuatu seimbang dalam bentuk, "
"formasi, value, warna, dsbg. Keseimbangan bisa simetrikal atau pas atau "
"asimetrikal dan ganjil. Obyek, value, warna, tekstur, bentuk, formasi, "
"dsbg., bisa digunakan untuk mendapatkan keseimbangan dalam komposisi."

#: tutorial-elements.xml:167(para)
msgid "Contrast is the juxtaposition of opposing elements"
msgstr "Kontras adalah penempatan dari dua elemen yang berlawanan."

#: tutorial-elements.xml:180(para)
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"Empasis digunakan untuk membuat beberapa bagian dari karya seni terlihat "
"mencolok dan menarik perhatian. Pusat perhatian atau focal point adalah "
"tempat dimana sesuatu yang menarik perhatian pertama."

#: tutorial-elements.xml:195(para)
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"Proporsi menjelaskan ukuran, lokasi, atau ukuran dari sesuatu dibandingkan "
"dengan yang lain."

#: tutorial-elements.xml:209(para)
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"Pola dihasilkan dengan mengulang elemen yang sama (garis, bentuk atau warna) "
"terus menerus."

#: tutorial-elements.xml:223(para)
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"Gradiasi dari ukuran dan arah menghasilkan perspektif linear. Gradiasi dari "
"warna dari warna hangat ke sejuk dan tone dari gelap ke terang menghasilkan "
"perspektif aerial. Gradiasi menambahkan ketertarikan dan pergerakan sebuah "
"bentuk. Sebuah gradiasi dari terang ke gelap akan membuat mata mengikuti "
"sebuah bentuk."

#: tutorial-elements.xml:241(para)
msgid "The combining of distinct elements to form a whole."
msgstr "Kombinasi dari elemen-elemen untuk menghasilkan sesuatu."

#: tutorial-elements.xml:251(title)
msgid "Bibliography"
msgstr "Daftar Pustaka"

#: tutorial-elements.xml:252(para)
msgid "This is a partial bibliography used to build this document."
msgstr ""
"Berikut adalah Daftar Pustaka yang digunakan untuk membuat dokumen ini."

#: tutorial-elements.xml:255(ulink)
msgid "http://www.makart.com/resources/artclass/EPlist.html"
msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#: tutorial-elements.xml:258(ulink)
msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#: tutorial-elements.xml:261(ulink)
msgid "http://www.johnlovett.com/test.htm"
msgstr "http://www.johnlovett.com/test.htm"

#: tutorial-elements.xml:265(ulink)
msgid "http://digital-web.com/articles/elements_of_design/"
msgstr "http://digital-web.com/articles/elements_of_design/"

#: tutorial-elements.xml:269(ulink)
msgid "http://digital-web.com/articles/principles_of_design/"
msgstr "http://digital-web.com/articles/principles_of_design/"

#: tutorial-elements.xml:274(para)
#, fuzzy
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url="
"\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. "
"Also, thanks to the Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics "
"people have submitted to that project."
msgstr ""
"Terimakasih sebesar-besarnya pada Linda Kim (<ulink url=\"http://www."
"redlucite.org\">http://www.redlucite.org</ulink>) karna membantu saya "
"(<ulink url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) dalam "
"tutorial ini. Juga, terimakasih kepada Open Clip Art Library (<ulink url="
"\"http://www.openclipart.org/\">http://www.openclipart.org/</ulink>) dan "
"insan-insan grafi yang ikut andil dalam proyek tersebut."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-elements.xml:0(None)
msgid "translator-credits"
msgstr "pengakuan untuk penerjemah"

#~ msgid "http://sanford-artedventures.com/study/study.html"
#~ msgstr "http://sanford-artedventures.com/study/study.html"

#~ msgid "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
#~ msgstr "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
